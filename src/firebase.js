import { initializeApp } from 'firebase';
const app = initializeApp({
  apiKey: "AIzaSyDBTzmoLaotca-nSw3YnF0p-7iiP5cknMw",
  authDomain: "online-member-registration.firebaseapp.com",
  databaseURL: "https://online-member-registration.firebaseio.com",
  projectId: "online-member-registration",
  storageBucket: "online-member-registration.appspot.com",
  messagingSenderId: "211859136634"
});

app.firestore().enablePersistence({ synchronizeTabs: true })
.catch(function(err) {
    if (err.code == 'failed-precondition') {
      console.log(err)
    } else if (err.code == 'unimplemented') {
      console.log(err)
    }
});


export const db = app.firestore();
export const registrantsCollection = db.collection('registrants');