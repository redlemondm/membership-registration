import './firebase'

import { library } from '@fortawesome/fontawesome-svg-core'

// internal icons
import { faCheck, faCheckCircle, faInfoCircle, faExclamationTriangle, faExclamationCircle,
    faArrowUp, faAngleRight, faAngleLeft, faAngleDown,
    faEye, faEyeSlash, faCaretDown, faCaretUp, faEdit, faCalendar, faSearch } from "@fortawesome/free-solid-svg-icons"

import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome"

library.add( faCheck, faCheckCircle, faInfoCircle, faExclamationTriangle, faExclamationCircle,
    faArrowUp, faAngleRight, faAngleLeft, faAngleDown,
    faEye, faEyeSlash, faCaretDown, faCaretUp, faEdit, faCalendar, faSearch)

Vue.component('vue-fontawesome', FontAwesomeIcon)

import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'

import { firestorePlugin } from 'vuefire'
import Buefy from 'buefy'

import App from './App.vue'

import 'buefy/dist/buefy.css'

Vue.use(firestorePlugin)
Vue.use(VueRouter)
Vue.use(Buefy, {
  defaultIconComponent: 'vue-fontawesome',
  defaultIconPack: 'fas',
})
Vue.use(Vuex)

import Home from './pages/Home.vue'
import List from './pages/List.vue'
import BatchStats from './pages/BatchStats.vue'
import Raffle from './pages/Raffle.vue'
import Raffle2 from './pages/Raffle2.vue'
import RaffleTens from './pages/RaffleTens.vue'
import RaffleTens2 from './pages/RaffleTens2.vue'
import MostSenior from './pages/MostSenior.vue'

const routes = [
  { path: '/', component: Home },
  { path: '/list', component: List },
  { path: '/biggest-batch', component: BatchStats },
  { path: '/most-senior', component: MostSenior },
  { path: '/major-raffle1', component: Raffle },
  { path: '/major-raffle2', component: Raffle2 },
  { path: '/door-prize1', component: RaffleTens },
  { path: '/door-prize2', component: RaffleTens2 },
]

const router = new VueRouter({
  routes // short for `routes: routes`
})


new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
