import { Bar } from 'vue-chartjs'

export default {
  extends: Bar,
  props: ['labels', 'data'],
  data: () => ({
    options: {
      responsive: true,
      maintainAspectRatio: false
    }
  }),
  mounted () {
    // Overwriting base render method with actual data.
    this.renderChart({
      labels: this.labels,
      datasets: [
        {
          label: 'Batch / Year',
          backgroundColor: '#f87979',
          data: this.data
        }
      ]
    }, this.options)
  }
}